import { test } from "@b08/test-runner";
import { importTypes, importIds } from "../src";
import { TypeId, ImportedType } from "@b08/type-parser";

test("importTypes should return imports to types and exclude type from same file", expect => {
  // arrange
  const from: TypeId = { name: "type", file: "file2", folder: "./src/folder1", isModulesPath: false };

  const types: ImportedType[] = [
    { alias: "a", id: { folder: "./src/folder1", file: "file2", name: "type2", isModulesPath: false } },
    { alias: "type4", id: { folder: "./src/folder1", file: "file3", name: "type3", isModulesPath: false } },
    { alias: "type5", id: { folder: "./src/folder1/folder2", file: "file1", name: "type5", isModulesPath: false } },
  ];

  const expected = [
    { alias: "type4", type: "type3", importPath: "./file3" },
    { alias: "type5", type: "type5", importPath: "./folder2/file1" }
  ];

  // act
  const result = importTypes(types, from);

  // assert
  expect.deepEqual(result, expected);
});


test("importIds should return imports to types and exclude type from same file", expect => {
  // arrange
  const from: TypeId = {
    name: "type",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const types: TypeId[] = [
    { folder: "./src/folder1", file: "file2", name: "type2", isModulesPath: false },
    { folder: "./src/folder1", file: "file3", name: "type3", isModulesPath: false },
    { folder: "./src/folder1/folder2", file: "file1", name: "type5", isModulesPath: false },
    { folder: "module/path", file: null, name: "type5", isModulesPath: true }
  ];

  const expected = [
    { alias: "type3", type: "type3", importPath: "./file3" },
    { alias: "type5", type: "type5", importPath: "./folder2/file1" },
    { alias: "type5", type: "type5", importPath: "module/path" }
  ];

  // act
  const result = importIds(types, from);

  // assert
  expect.deepEqual(result, expected);
});
