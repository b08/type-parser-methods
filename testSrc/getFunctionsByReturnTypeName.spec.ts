import { test } from "@b08/test-runner";
import { getFunctionsByReturnTypeName } from "../src";

test("functions by return type", expect => {
  // arrange
  const returnType = {
    typeName: "Cls"
  };

  const returnType2 = {
    typeName: "Cls2"
  };

  const func1 = [{ returnType }, { returnType: returnType2 }, { returnType }];
  const func2 = [{ returnType }, { returnType: returnType2 }, { returnType: returnType2 }];

  const models = [{ functions: func1 }, { functions: func2 }];

  // act
  const result = getFunctionsByReturnTypeName(<any>returnType, <any>models);

  // assert
  const expected = [func1[0], func1[2], func2[0]];
  expected.forEach((item, index) => {
    expect.equal(item, result[index]);
  });
});
