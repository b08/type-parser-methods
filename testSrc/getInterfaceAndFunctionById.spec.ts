import { test } from "@b08/test-runner";
import { TypeId, InterfaceModel, Synonym, TypesModel, FunctionModel } from "@b08/type-parser";
import { getInterfaceById, getFunctionById } from "../src";

test("should return function by synonym", expect => {
  // arrange
  const id: TypeId = {
    name: "Func",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const searchId: TypeId = {
    name: "Func",
    file: "folder1",
    folder: "./src",
    isModulesPath: false
  };

  const func: FunctionModel = {
    id,
    comments: [],
    parameters: [],
    returnType: null,
    async: false
  };

  const synonyms: Synonym[] = [
    { file: "file1", },
    { file: "file2", },
    { file: "file3", },
  ];

  const models: TypesModel[] = [
    {
      classes: [],
      synonyms: [],
      file: <any>{ folder: "./src/folder1", name: "file2" },
      interfaces: [],
      enums: [],
      functions: [func],
      constants: [],
      types: [],
    },
    {
      classes: [],
      synonyms,
      file: <any>{ folder: "./src/folder1", name: "index" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    }
  ];

  // act
  const result = getFunctionById(searchId, models);

  // assert
  expect.equal(result, func);
});


test("should return interface by synonym", expect => {
  // arrange
  const id: TypeId = {
    name: "Intf",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const searchId: TypeId = {
    name: "Intf",
    file: "folder1",
    folder: "./src",
    isModulesPath: false
  };

  const intf: InterfaceModel = {
    id,
    comments: [],
    methods: null,
    fields: null,
    extends: null,
  };

  const synonyms: Synonym[] = [
    { file: "file1", },
    { file: "file2", },
    { file: "file3", },
  ];

  const models: TypesModel[] = [
    {
      classes: [],
      synonyms: [],
      file: <any>{ folder: "./src/folder1", name: "file2" },
      interfaces: [intf],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    },
    {
      classes: [],
      synonyms,
      file: <any>{ folder: "./src/folder1", name: "index" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    }
  ];

  // act
  const result = getInterfaceById(searchId, models);

  // assert
  expect.equal(result, intf);
});
