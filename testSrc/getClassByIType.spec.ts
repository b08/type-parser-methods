import { test } from "@b08/test-runner";
import { ClassModel, Synonym, TypeDefinition, TypeId, TypesModel } from "@b08/type-parser";
import { getClassByType } from "../src";

test("getClassByType should return class by field type", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const fieldType: TypeDefinition = {
    typeName: "Cls",
    importedTypes: [{
      id,
      alias: ""
    }]
  };

  const cls: ClassModel = {
    id,
    comments: [],
    constructor: null,
    methods: null,
    fields: null,
    extends: null,
    implements: null
  };

  const model: TypesModel = {
    classes: [cls],
    interfaces: [],
    enums: [],
    functions: [],
    constants: [],
    synonyms: [],
    types: [],
    file: null
  };

  // act
  const result = getClassByType(fieldType, [model]);

  // assert
  expect.equal(result, cls);
});


test("getClassByType should return class by synonym", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const searchId: TypeId = {
    name: "Cls",
    file: "folder1",
    folder: "./src",
    isModulesPath: false
  };

  const fieldType: TypeDefinition = {
    typeName: "Cls",
    importedTypes: [{
      id: searchId,
      alias: ""
    }]
  };

  const cls: ClassModel = {
    id,
    comments: [],
    constructor: null,
    methods: null,
    fields: null,
    extends: null,
    implements: null
  };

  const synonyms: Synonym[] = [
    { file: "file1", },
    { file: "file2", },
    { file: "file3", },
  ];

  const models: TypesModel[] = [
    {
      classes: [cls],
      synonyms: [],
      file: <any>{ folder: "./src/folder1", name: "file2" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    },
    {
      classes: [],
      synonyms,
      file: <any>{ folder: "./src/folder1", name: "index" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    }
  ];

  // act
  const result = getClassByType(fieldType, models);

  // assert
  expect.equal(result, cls);
});
