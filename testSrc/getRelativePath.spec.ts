import { test } from "@b08/test-runner";
import { getRelativePath } from "../src";
import { TypeId } from "@b08/type-parser";

test("getRelativePath should return path to neighbor file", expect => {
  // arrange

  const source = "./src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const expected = "./file2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return dot for neighbor index", expect => {
  // arrange

  const source = "./src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "index",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const expected = ".";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return path to child folder file", expect => {
  // arrange

  const source = "./src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "file2",
    folder: "./src/folder1/folder2",
    isModulesPath: false
  };

  const expected = "./folder2/file2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return path to child folder for index", expect => {
  // arrange

  const source = "./src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "index",
    folder: "./src/folder1/folder2",
    isModulesPath: false
  };

  const expected = "./folder2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should work for linux paths with leading slash and without", expect => {
  // arrange

  const source = "mnt/c/src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "file2",
    folder: "/mnt/c/src/folder1/folder2",
    isModulesPath: false
  };

  const expected = "./folder2/file2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return path to neighbor folder file", expect => {
  // arrange

  const source = "./src/src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "file2",
    folder: "./src/folder1/folder2",
    isModulesPath: false
  };

  const expected = "../../folder1/folder2/file2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return path to neighbor folder for index", expect => {
  // arrange

  const source = "./src/src/folder1";

  const dest: TypeId = {
    name: "type",
    file: "index",
    folder: "./src/folder1/folder2",
    isModulesPath: false
  };

  const expected = "../../folder1/folder2";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});

test("getRelativePath should return path to modules as is", expect => {
  // arrange

  const source = "./src/src/folder1";

  const dest: TypeId = {
    name: "type",
    file: null,
    folder: "module/path",
    isModulesPath: true
  };

  const expected = "module/path";

  // act
  const result = getRelativePath(source, dest);

  // assert
  expect.equal(result, expected);
});
