import { test } from "@b08/test-runner";
import { TypeId } from "@b08/type-parser";
import { isSameType } from "../src";

test("isSameType returns true for same type", expect => {
  // arrange
  const type: TypeId = {
    name: "type",
    folder: "folder",
    file: "file",
    isModulesPath: false
  };

  // act
  const result = isSameType(type, type);

  // assert
  expect.true(result);
});

test("isSameType returns false for another file", expect => {
  // arrange
  const type: TypeId = {
    name: "type",
    folder: "folder",
    file: "file",
    isModulesPath: false
  };

  // act
  const result = isSameType(type, type);

  // assert
  expect.true(result);
});


test("isSameType returns true for imported from folder index", expect => {
  // arrange
  const type1: TypeId = {
    name: "type",
    folder: "folder/subfolder",
    file: "file",
    isModulesPath: false
  };

  const type2: TypeId = {
    name: "type",
    folder: "folder",
    file: "subfolder",
    isModulesPath: false
  };

  // act
  const result = isSameType(type1, type2);

  // assert
  expect.true(result);
});
