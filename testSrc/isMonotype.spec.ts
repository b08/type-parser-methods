import { test } from "@b08/test-runner";
import { isMonotype } from "../src";
import { TypeDefinition } from "@b08/type-parser";

test("isMonotype should return true for string", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "string",
    importedTypes: []
  };
  // act
  const result = isMonotype(type);

  // assert
  expect.true(result);
});

test("isMonotype should return true for monotype", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "Type",
    importedTypes: [{ alias: "Type", id: null }]
  };
  // act
  const result = isMonotype(type);

  // assert
  expect.true(result);
});

test("isMonotype should return false for complex type", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "Type<string>",
    importedTypes: [{ alias: "Type", id: null }]
  };
  // act
  const result = isMonotype(type);

  // assert
  expect.false(result);
});

