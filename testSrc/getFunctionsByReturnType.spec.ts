import { TypeId, TypeDefinition } from "@b08/type-parser";
import { test } from "@b08/test-runner";
import { getFunctionsByReturnType } from "../src";

test("functions by return type", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const id2: TypeId = {
    name: "Cls2",
    file: "file23",
    folder: "./src/folder14",
    isModulesPath: false
  };

  const returnType: TypeDefinition = {
    typeName: "Cls",
    importedTypes: [{ alias: "Cls", id }]
  };

  const returnType2: TypeDefinition = {
    typeName: "Cls2",
    importedTypes: [{ alias: "Cls2", id: id2 }]
  };

  const func1 = [{ returnType }, { returnType: returnType2 }, { returnType }];
  const func2 = [{ returnType }, { returnType: returnType2 }, { returnType: returnType2 }];

  const models = [{ functions: func1 }, { functions: func2 }];

  // act
  const result = getFunctionsByReturnType(returnType, <any>models);

  // assert
  const expected = [func1[0], func1[2], func2[0]];
  expected.forEach((item, index) => {
    expect.equal(item, result[index]);
  });
});
