import { test } from "@b08/test-runner";
import { InterfaceModel, TypeId, TypesModel } from "@b08/type-parser";
import { interfaces } from "../src";

const lambda: (c: InterfaceModel) => boolean = c => c.id != null;
function interfacesWithId(models: TypesModel[]): InterfaceModel[] {
  return interfaces(models, lambda);
}

test("interfaces should return interfaces satisfying the lambda", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const int1 = [{ id }, {}, { id }];
  const int2 = [{ id }, { id }, {}];

  const models = [{ interfaces: int1 }, { interfaces: int2 }];

  // act
  const result = interfacesWithId(<any>models);
  const result2 = interfacesWithId(<any>models);

  // assert
  expect.equal(result, result2);
  const expected = [int1[0], int1[2], int2[0], int2[1]];
  expected.forEach((item, index) => {
    expect.equal(item, result[index]);
  });
});
