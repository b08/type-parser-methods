import { test } from "@b08/test-runner";
import { TypeDefinition } from "@b08/type-parser";
import { isImportableMonotype } from "../src";

test("isImportableMonotype should return false for string", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "string",
    importedTypes: []
  };
  // act
  const result = isImportableMonotype(type);

  // assert
  expect.false(result);
});

test("isImportableMonotype should return true for importable monotype", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "Type",
    importedTypes: [{ alias: "Type", id: null }]
  };
  // act
  const result = isImportableMonotype(type);

  // assert
  expect.true(result);
});

test("isImportableMonotype should return false for complex type", expect => {
  // arrange
  const type: TypeDefinition = {
    typeName: "Type<string>",
    importedTypes: [{ alias: "Type", id: null }]
  };
  // act
  const result = isImportableMonotype(type);

  // assert
  expect.false(result);
});

