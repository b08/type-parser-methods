import { test } from "@b08/test-runner";
import { TypeId, ClassModel, TypesModel, Synonym } from "@b08/type-parser";
import { getClassById } from "../src";

test("getClassById should return class by its id", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const cls: ClassModel = {
    id,
    comments: [],
    constructor: null,
    methods: null,
    fields: null,
    extends: null,
    implements: null
  };

  const model: TypesModel = {
    classes: [cls],
    interfaces: null,
    enums: null,
    functions: null,
    constants: null,
    synonyms: [],
    types: null,
    file: null
  };

  // act
  const result = getClassById(id, [model]);

  // assert
  expect.equal(result, cls);
});


test("getClassById should return class by synonym", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const searchId: TypeId = {
    name: "Cls",
    file: "folder1",
    folder: "./src",
    isModulesPath: false
  };

  const cls: ClassModel = {
    id,
    comments: [],
    constructor: null,
    methods: null,
    fields: null,
    extends: null,
    implements: null
  };

  const synonyms: Synonym[] = [
    { file: "file1", },
    { file: "file2", },
    { file: "file3", },
  ];

  const models: TypesModel[] = [
    {
      classes: [cls],
      synonyms: [],
      file: <any>{ folder: "./src/folder1", name: "file2" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    },
    {
      classes: [],
      synonyms,
      file: <any>{ folder: "./src/folder1", name: "index" },
      interfaces: [],
      enums: [],
      functions: [],
      constants: [],
      types: [],
    }
  ];

  // act
  const result = getClassById(searchId, models);

  // assert
  expect.equal(result, cls);

});
