import { test } from "@b08/test-runner";
import { FunctionModel, TypeId, TypesModel } from "@b08/type-parser";
import { functions } from "../src";

const lambda: (c: FunctionModel) => boolean = c => c.id != null;
function functionsWithId(models: TypesModel[]): FunctionModel[] {
  return functions(models, lambda);
}

test("functions should return functions satisfying the lambda", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const func1 = [{ id }, {}, { id }];
  const func2 = [{ id }, { id }, {}];

  const models = [{ functions: func1 }, { functions: func2 }];

  // act
  const result = functionsWithId(<any>models);
  const result2 = functionsWithId(<any>models);

  // assert
  expect.equal(result, result2);
  const expected = [func1[0], func1[2], func2[0], func2[1]];
  expected.forEach((item, index) => {
    expect.equal(item, result[index]);
  });
});
