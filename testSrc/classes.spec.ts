import { test } from "@b08/test-runner";
import { ClassModel, TypeId, TypesModel } from "@b08/type-parser";
import { classes } from "../src";

const lambda: (c: ClassModel) => boolean = c => c.id != null;
function classesWithId(models: TypesModel[]): ClassModel[] {
  return classes(models, lambda);
}

test("classes should return classes satisfying the lambda", expect => {
  // arrange
  const id: TypeId = {
    name: "Cls",
    file: "file2",
    folder: "./src/folder1",
    isModulesPath: false
  };

  const classes1 = [{ id }, {}, { id }];
  const classes2 = [{ id }, { id }, {}];

  const models = [{ classes: classes1 }, { classes: classes2 }];

  // act
  const result = classesWithId(<any>models);
  const result2 = classesWithId(<any>models);

  // assert
  expect.equal(result, result2);
  const expected = [classes1[0], classes1[2], classes2[0], classes2[1]];
  expected.forEach((item, index) => {
    expect.equal(item, result[index]);
  });
});
