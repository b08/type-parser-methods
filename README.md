# @b08/type-parser-methods, seeded from @b08/library-seed, library type: feature
A set of functions to be used with type-parser result

# exported types 
Returns type ids of all types exported from file

# getRelativePath
Input paramets are - current folder and TypeId to import
Returns a path to use in import statement

# importId and importIds
Input parameters - type to import from, and type(s) to import
Returns an import model to use with imports-generator

# importType and importTypeDefinition
Same as previous, these functions extract needed ids from type and type definitions and create all needed import models

# isMonotype
Returns true if type definition is for a mono type, like "string" or "User" and false for complex types like generics or arrays

# isImportableMonotype
Returns true for type definitions having a monotype with importable type.

# resolvePath
This function is not actually for using with type-parser result, but with external paths. It resolves the path against cwd and replaces left slashes with right ones.

# resolveType
Returns a type with resolved "folder" field.

# classes, functions and interfaces
Memoized functions that receive an array of parsed models and a predicate and return a list of classes, function and interfaces respectively. Do not use in-place lambda here, use lambdas or functions that are constant in context.
Example:
```
function isGoodClass(cls: ClassModel) { return !!cls.id.folder.match(/myFolder/); }

function myGoodClasses(parsed: ParsedModel[]): ClassModel[] {
  return classes(parsed, isGoodClass);
}
```

# getOriginalId
in case typeId points at a bucket file with reference like this: import * from "./file" it will transform type id to an actual id of the file, otherwise will return input id

# getClassById, getInterfaceById and getFunctionById
As the names imply, these functions return corresponding type by its id from array of parsed files.
Includes bucket refernces.

# getClassByType, getInterfaceByType and getFunctionByType
Same as by id, but by type of field or function parameter or return type. Check for isImportableMonotype prior to calling these, as no internal check is performed, and might result in error.

# getFunctionsByReturnType
Works only for importable monotypes. Same as "functions", can receive a constant predicate.

# getFunctionsByReturnTypeName
Works for any type, but can have wrong results if parsed files have types with the same name.
