import { unique } from "@b08/flat-key";
import { filterWith, mapWith } from "@b08/functional";
import { getRelativePath } from "./getRelativePath";
import { flatMap } from "@b08/array";
import { TypeDefinition, TypeId, ImportedType } from "@b08/type-parser";

export const importTypeDefinitions = (def: TypeDefinition[], from: TypeId) =>
  importTypes(flatMap(def, d => d.importedTypes), from);

export const importTypeDefinition = (def: TypeDefinition, from: TypeId) =>
  importTypes(def.importedTypes, from);

export const importTypes = (types: ImportedType[], from: TypeId) =>
  unique(mapWith(importType)(typesInOtherFiles(types, from), from));

export const importType = (type: ImportedType, from: TypeId) =>
  ({ alias: type.alias, type: type.id.name, importPath: getRelativePath(from.folder, type.id) });

export const importIds = (ids: TypeId[], from: TypeId) =>
  unique(mapWith(importId)(idsInOtherFiles(ids, from), from));

export const importId = (id: TypeId, from: TypeId) =>
  ({ type: id.name, alias: id.name, importPath: getRelativePath(from.folder, id) });

const typesInOtherFiles = filterWith((imported: ImportedType, from: TypeId) => isInOtherFile(imported.id, from));

const idsInOtherFiles = filterWith(isInOtherFile);

function isInOtherFile(id: TypeId, from: TypeId): boolean {
  return id.file !== from.file || id.folder !== from.folder;
}

