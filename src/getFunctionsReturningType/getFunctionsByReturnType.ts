import { TypeId, TypesModel, FunctionModel, TypeDefinition } from "@b08/type-parser";
import { isImportableMonotype } from "../isMonotype";
import { memoize } from "@b08/memoize";
import { groupBy } from "@b08/flat-key";
import { functions } from "../getTypes/functions";
import { TypePredicate } from "../getTypes/types";

export function getFunctionsByReturnType(type: TypeDefinition, parsed: TypesModel[], predicate?: TypePredicate<FunctionModel>): FunctionModel[] {
  if (!isImportableMonotype(type)) { return null; }
  return getFunctionsByReturnTypeId(type.importedTypes[0].id, parsed, predicate);
}

export function getFunctionsByReturnTypeId(type: TypeId, parsed: TypesModel[], predicate?: TypePredicate<FunctionModel>): FunctionModel[] {
  return functionsGroup(parsed, predicate).get(type);
}

const returnsImportableMonotype = (func: FunctionModel) => isImportableMonotype(func.returnType);
const functionsGroup = memoize((parsed: TypesModel[], predicate?: TypePredicate<FunctionModel>) => {
  let funcs = functions(parsed, returnsImportableMonotype);
  if (predicate != null) { funcs = funcs.filter(predicate); }
  return groupBy(funcs, f => f.returnType.importedTypes[0].id);
});


