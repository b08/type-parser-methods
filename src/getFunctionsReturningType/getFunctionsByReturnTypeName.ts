import { memoize } from "@b08/memoize";
import { TypesModel, TypeDefinition, FunctionModel } from "@b08/type-parser";
import { groupBy } from "@b08/flat-key";
import { functions } from "../getTypes/functions";

export function getFunctionsByReturnTypeName(type: TypeDefinition, parsed: TypesModel[]): FunctionModel[] {
  return functionsGroup(parsed).get(type.typeName);
}

const functionsGroup = memoize((parsed: TypesModel[]) => {
  return groupBy(functions(parsed), f => f.returnType.typeName);
});
