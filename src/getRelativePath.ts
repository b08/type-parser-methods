import { TypeId } from "@b08/type-parser";

export function getSplit(path: string): string[] {
  return path.split(/[\\\/]+/).filter(s => s.length > 0);
}

export function getRelativePath(sourceFolder: string, destinationType: TypeId): string {
  if (destinationType.isModulesPath) { return destinationType.folder; }
  const splitSource = getSplit(sourceFolder);
  const splitDest = getSplit(destinationType.folder);
  const startSegment = getFirstNonMatchingSegment(splitSource, splitDest);
  const wayUp = splitSource.slice(startSegment).map(_ => "..").join("/");
  const wayDown = splitDest.slice(startSegment).map(s => "/" + s).join("");
  const file = destinationType.file === "index" || destinationType.file === "" || destinationType.file == null
    ? ""
    : "/" + destinationType.file;
  return wayUp.length === 0
    ? "." + wayDown + file
    : wayUp + wayDown + file;
}


function getFirstNonMatchingSegment(source: string[], dest: string[]): number {
  for (let i = 0; i < source.length; i++) {
    if (dest.length < i + 1 || dest[i] !== source[i]) { return i; }
  }

  return source.length;
}
