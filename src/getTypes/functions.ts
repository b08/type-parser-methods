import { memoize } from "@b08/memoize";
import { FunctionModel, TypesModel } from "@b08/type-parser";
import { TypePredicate, types } from "./types";

const functionsSelector = (model: TypesModel) => model.functions;
export const functions = memoize((models: TypesModel[], predicate?: TypePredicate<FunctionModel>) =>
  types(models, functionsSelector, predicate));
