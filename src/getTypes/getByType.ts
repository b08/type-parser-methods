import { TypeDefinition, TypesModel } from "@b08/type-parser";
import { getClassById, getInterfaceById, getFunctionById } from "./getById";

export const getClassByType = (type: TypeDefinition, models: TypesModel[]) =>
  getClassById(type.importedTypes[0].id, models);

export const getInterfaceByType = (type: TypeDefinition, models: TypesModel[]) =>
  getInterfaceById(type.importedTypes[0].id, models);

export const getFunctionByType = (type: TypeDefinition, models: TypesModel[]) =>
  getFunctionById(type.importedTypes[0].id, models);
