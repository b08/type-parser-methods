import { memoize } from "@b08/memoize";
import { InterfaceModel, TypesModel } from "@b08/type-parser";
import { TypePredicate, types } from "./types";

const interfacesSelector = (model: TypesModel) => model.interfaces;
export const interfaces = memoize((models: TypesModel[], predicate?: TypePredicate<InterfaceModel>) =>
  types(models, interfacesSelector, predicate));
