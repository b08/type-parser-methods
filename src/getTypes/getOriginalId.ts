import { flatMap } from "@b08/array";
import { mapBy } from "@b08/flat-key";
import { memoize } from "@b08/memoize";
import { TypeId, TypesModel } from "@b08/type-parser";
import { exportedTypeIds } from "../exportedTypes";

export function getOriginalId(id: TypeId, models: TypesModel[]): TypeId {
  if (id.isModulesPath) { return id; }
  const map = synonymMap(models);
  return map.get(id) || id;
}

const synonymMap = memoize((models: TypesModel[]) => {
  const synonyms = flatMap(models, m => getSynonyms(m, models));
  return mapBy(synonyms, s => s.imported, s => s.target);
});

interface Synonym {
  imported: TypeId;
  target: TypeId;
}

function getSynonyms(model: TypesModel, models: TypesModel[]): Synonym[] {
  if (model.synonyms.length === 0) { return []; }
  const index = model.file.folder.lastIndexOf("/");
  const from = {
    folder: model.file.folder.substr(0, index),
    file: model.file.folder.substr(index + 1),
    isModulesPath: false
  };
  const indexId = {
    folder: model.file.folder,
    file: "index",
    isModulesPath: false
  };

  const files = models.filter(m => isReferencedBy(m, model));
  const exportedIds = flatMap(files, exportedTypeIds);
  return flatMap(exportedIds, target => [
    {
      imported: { ...from, name: target.name },
      target
    },
    {
      imported: { ...indexId, name: target.name },
      target
    }
  ]);
}

function isReferencedBy(model: TypesModel, reference: TypesModel): boolean {
  const sameFolder = reference.file.folder === model.file.folder;
  const nameReferenced = reference.synonyms.some(s => s.file === model.file.name);
  return sameFolder && nameReferenced;
}
