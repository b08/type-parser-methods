import { flatMap } from "@b08/array";
import { mapBy } from "@b08/flat-key";
import { memoize } from "@b08/memoize";
import { ClassModel, FunctionModel, InterfaceModel, TypeId, TypesModel } from "@b08/type-parser";
import { getOriginalId } from "./getOriginalId";

export function getClassById(id: TypeId, models: TypesModel[]): ClassModel {
  return classesMap(models).get(getOriginalId(id, models));
}

const classesMap = memoize((models: TypesModel[]) => mapBy(flatMap(models, m => m.classes), c => c.id));

export function getInterfaceById(id: TypeId, models: TypesModel[]): InterfaceModel {
  return interfacesMap(models).get(getOriginalId(id, models));
}

const interfacesMap = memoize((models: TypesModel[]) => mapBy(flatMap(models, m => m.interfaces), c => c.id));

export function getFunctionById(id: TypeId, models: TypesModel[]): FunctionModel {
  return functionsMap(models).get(getOriginalId(id, models));
}

const functionsMap = memoize((models: TypesModel[]) => mapBy(flatMap(models, m => m.functions), c => c.id));



