import { TypeModel, TypesModel } from "@b08/type-parser";
import { flatMap } from "@b08/array";

export type TypeSelector<T> = (model: TypesModel) => T[];
export type TypePredicate<T> = (type: T) => boolean;

export function types<T extends TypeModel>(models: TypesModel[], selector: TypeSelector<T>, predicate?: TypePredicate<T>): T[] {
  return predicate == null
    ? flatMap(models, selector)
    : flatMap(models, selector).filter(predicate);
}
