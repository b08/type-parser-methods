import { memoize } from "@b08/memoize";
import { ClassModel, TypesModel } from "@b08/type-parser";
import { types, TypePredicate } from "./types";

const classSelector = (model: TypesModel) => model.classes;
export const classes = memoize((models: TypesModel[], predicate?: TypePredicate<ClassModel>) =>
  types(models, classSelector, predicate));
