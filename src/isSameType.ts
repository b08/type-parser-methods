import { TypeId } from "@b08/type-parser";

export function isSameType(type1: TypeId, type2: TypeId): boolean {
  return type1.isModulesPath === type2.isModulesPath
    && type1.name === type2.name
    && (importedDirectly(type1, type2)
      || importedFromIndex(type1, type2)
      || importedFromIndex(type2, type1));
}

function importedDirectly(type1: TypeId, type2: TypeId): boolean {
  return type1.file === type2.file && type1.folder === type2.folder;
}

function importedFromIndex(type1: TypeId, type2: TypeId): boolean {
  return !type1.isModulesPath && importedFromIndexFile(type1, type2) || importedFromFolder(type1, type2);
}

function importedFromIndexFile(type1: TypeId, type2: TypeId): boolean {
  return type1.folder === type2.folder && type1.file === "index";
}
function importedFromFolder(type1: TypeId, type2: TypeId): boolean {
  return `${type1.folder}/${type1.file}` === type2.folder;
}
