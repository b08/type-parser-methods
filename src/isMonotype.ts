import { TypeDefinition } from "@b08/type-parser";

const complexTypeRegex = /[\[\]<>{}]/;
export function isMonotype(field: TypeDefinition): boolean {
  return field.importedTypes.length < 2
    && !field.typeName.match(complexTypeRegex);
}

export function isImportableMonotype(field: TypeDefinition): boolean {
  return field.importedTypes.length === 1
    && !field.typeName.match(complexTypeRegex);
}
