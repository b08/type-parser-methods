import { resolve } from "path";
import { mapWith } from "@b08/functional";
import { TypeId } from "@b08/type-parser";

export const resolvePath = (path: string) => path && resolve(path).replace(/[\\\/]+/, "/");

export const resolveType = (type: TypeId) => type.isModulesPath ? type : { ...type, folder: resolvePath(type.folder) };

export const resolveTypes = mapWith(resolveType);
